import storePlugin from './store';

const plugins = {
  elastic_store: storePlugin,
};

export { plugins };
export default { plugins };
