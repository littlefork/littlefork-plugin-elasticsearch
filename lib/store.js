import {merge, size} from 'lodash/fp';
import Promise from 'bluebird';
import {envelope as env} from 'littlefork';
import elastic from 'elasticsearch';
// import bodybuilder from 'bodybuilder';

const client = new elastic.Client({
  host: 'localhost:9200',
  log: 'warning',
  defer: Promise.defer,
});

const toHeader = u => ({
  index: 'littlefork',
  type: 'data unit',
  id: u._lf_id_hash,
});

const toMsg = u => merge(toHeader(u), {body: u});

const storeData = u =>
  client.exists(toHeader(u))
      .then(exists => (!exists ? client.create(toMsg(u)) : null))
      .return(u);

const plugin = (envelope, {log}) => {
  log.info(`Writing ${size(envelope.data)} units to Elasticsearch.`);

  // const body = bodybuilder().query('match', 'message', 'this is a test');

  return env.fmapDataAsync(storeData, envelope).return(envelope);
  // return client.create({
  //   body:
  // })
  //   //.ping({requestTimeout: 30000})
  //   // .then(() => client.search({body: body.build()}))
  //   .tap(console.log)
  //   .return(envelope);
};

plugin.desc = 'Store units of data in Elasticsearch.';

export default plugin;
